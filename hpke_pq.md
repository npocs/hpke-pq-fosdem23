---
author: Norbert Pócs
title: Converting HPKE to be PQ
institute: Red Hat
date: February 4 2023
theme: metropolis
aspectratio: 169

---


# Overview

**H**ybrid
**P**ublic
**K**ey
**E**ncryption

* asymmetric + symmetric encryption
* scheme using KEM (Key Encapsulation Mechanism)
* RFC 9180 [@rfc9180]


# Fundamental parts

::: columns

:::: {.column width=15%}
**KEM**

P-256\
P-384\
P-521\
X25519\
X448
::::

:::: {.column width=25%}
**Key Schedule**

HKDF-SHA256\
HKDF-SHA384\
HKDF-SHA512
::::

:::: {.column width=20%}
**AEAD**

AES-128-GCM\
AES-256-GCM\
ChaCha20Poly1305
::::

:::

# Fundamental parts

::: columns

:::: {.column width=30%}
## KEM operations
* Key generation
* Encapsulation
* Decapsulation
::::

:::: {.column width=30%}
## Key schedule operations
* KDF (Key Derivation Function)
  * Extract
  * Expand
::::

:::: {.column width=30%}
## AEAD operations
* Seal
* Open
::::

:::


# The mechanism itself
![](mechanism0.png)


# The mechanism itself

![HPKE overview [@hpke_pq]](mechanism1.png){width=65%}


# Use cases, applications

### Possible usages
* Messaging layer Security (MLS)
* TLS ClientHello
* Oblivious DNS over HTTPS (ODoH)


# Modes

### HPKE modes

* Base mode
* Authentication modes:
  * Auth mode
  * PSK mode
  * AuthPSK mode


# Security of HPKE

* Base mode
  * IND-CCA2 secure [^1]
* Authenticated modes
  * Outsider-CCA secure
  * Insider-CCA secure [^2]


[^1]: Full report here [@sec_analysis20]
[^2]: Full report here [@sec_analysis21]


# PQ HPKE

::: columns

:::: {.column width=30%}
**KEM**

P-256, ..\
X25519, ..\
**+**\
Kyber\
~~SIKE~~
::::

:::: {.column width=50%}
* SIKE is out of game
* Kyber is one of the NIST finalists for KEX
* KEM instead of DH style KEX
* lattice based scheme - learning with errors and rounding problem (MLWER)
* IND-CCA2 secure

::::

:::


# Hybrid PQ HPKE

![PQ-only and PQ-hybrid HPKE Overview [@hpke_pq]](mechanism2.png){width=75%}


# Security of PQ versions

* PQ only Base mode is still IND-CCA2 secure as long as the underlying KEM is IND-CCA2 safe
* PQ hybrid needs more proof
* Authentication mode for both would need more work to prove Outsider-CCA and Insider-CCA


# Benchmarks

## HW of the test machine
Intel(R) Core(TM) i7-10610U CPU (4 cores and 8MB of cache) with 8GB of RAM running 1.80GHz with
maximum turbo frequency of up to 4.90 GHz. For our implementations we used the AWS-LC cryptographic library. [^4]

## Test variables
We ran our experiments for each algorithm 1,000 times. To
increase our accuracy, we eliminated the first and fourth quartile
of our measurements. Additionally, all our results include the
mean of the measured algorithm in CPU clock cycles. [^4]

[^4]: [@hpke_pq]


# Benchmarks graphs

![Classical vs PQ HPKE Performance [@hpke_pq]](graphs0.png){width=90%}


# Benchmarks graphs

![Performance Breakdown for Classical and PQ-hybrid HPKE[@hpke_pq]](graphs1.png){width=40%}


# References

::: {#refs}
:::

# 

## Thank you for attention!
