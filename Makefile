NAME=out

$(NAME).pdf: hpke_pq.md references.bib
	pandoc --to=beamer --output=out.pdf hpke_pq.md --bibliography references.bib --citeproc

clean:
	rm -f $(NAME).pdf
